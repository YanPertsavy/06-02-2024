#include "header06.02.24.h"
#include <iostream>
using namespace std;

int createEmptyMassive(int length)
{
    int* arr = new int[length];
    return *arr;
}

int createRandomMassive(int length)
{
    int* arr = new int[length];
    srand(time(NULL));
    for (size_t i = 0; i < length; i++)
    {
        arr[i] = rand() % 200 - 100;
    }
    return *arr;
}

int resizeOldMassive(int oldLength, int* oldArr, int newLength)
{
    int* newArr = new int[newLength];
    for (size_t i = 0; i < oldLength; i++)
    {
        newArr[i] = oldArr[i];
    }
    delete[] oldArr;
    return *newArr;

}

int addElementToEnd(int oldLength, int* oldArr, int element)
{
    int newLength = oldLength + 1;
    int* newArr = new int[newLength];
    for (size_t i = 0; i < newLength; i++)
    {
        if (i == newLength - 1)
        {
            newArr[i] = element;
            break;
        }
        newArr[i] = oldArr[i];

    }
    return *newArr;

}

int deleteElementFromEnd(int oldLength, int* oldArr)
{
    int newLength = oldLength - 1;
    int* newArr = new int[newLength];
    for (size_t i = 0; i < newLength; i++)
    {
        newArr[i] = oldArr[i];
    }
    return *newArr;
}

int addElementToStart(int oldLength, int* oldArr, int element)
{
    int newLength = oldLength + 1;
    int* newArr = new int[newLength];
    newArr[0] = element;
    for (int i = 1, l = 0; i < newLength; i++, l++)
    {
        newArr[i] = oldArr[l];
    }
    return *newArr;
}

int deleteElementFromStart(int oldLength, int* oldArr)
{
    int newLength = oldLength - 1;
    int* newArr = new int[newLength];
    for (int i = 0, f = 1; i < newLength; i++, f++)
    {
        newArr[i] = oldArr[f];
    }
    return *newArr;
}

int addElementToSelectedPosition(int oldLength, int* oldArr, int element, int position)
{
    int newLength = oldLength + 1;
    int* newArr = new int[newLength];
    for (int i = 0, f = 0; i < newLength; i++, f++)
    {
        newArr[i] = oldArr[f];
        if (i == position)
        {
            newArr[i] = element;
            f--;
        }
    }
    return *newArr;
}

int deleteElementFromSelectedPosition(int oldLength, int* oldArr, int position)
{
    int newLength = oldLength - 1;
    int* newArr = new int[newLength];
    for (int i = 0, f = 0; i < newLength; i++, f++)
    {
        newArr[i] = oldArr[f];

    }
    for (int h = position, k = position + 1; h < newLength; h++, k++)
    {
        newArr[h] = oldArr[k];
    }
    return *newArr;
}

int calculatingArithmeticMean(int* arr, int length)
{
    int sum = 0;
    for (int i = 0; i < length; i++)
    {
        sum += arr[i];
    }
    double arif = (sum / length);
    return arif;
}

int sumElementsOfMassive(int* arr, int length)
{
    double sum = 0;
    for (int i = 0; i < length; i++)
    {
        sum += arr[i];
    }
    return sum;
}

int findMinMaxElementFromMassive(int* arr, int length)
{
    int* MinMaxArr = new int[1];
    int max = arr[0];
    int min = arr[0];
    for (size_t i = 0; i < length; i++)
    {
        if (max < arr[i])
        {
            max = arr[i];
        }
        if (arr[i] < min)
        {
            min = arr[i];
        }

    }
    MinMaxArr[0] = min;
    MinMaxArr[1] = max;
    return *MinMaxArr;
}


