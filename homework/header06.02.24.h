#pragma once
namespace WorkingWithArrayHomework
{
	int createEmptyMassive(int);
	int createRandomMassive(int);
	int resizeOldMassive(int, int*, int);
	int addElementToEnd(int oldLength, int* oldArr, int element);
	int deleteElementFromEnd(int oldLength, int* oldArr);
	int addElementToStart(int oldLength, int* oldArr, int element);
	int deleteElementFromStart(int oldLength, int* oldArr);
	int addElementToSelectedPosition(int oldLength, int* oldArr, int element, int position);
	int deleteElementFromSelectedPosition(int oldLength, int* oldArr, int position);
	int calculatingArithmeticMean(int* arr, int length);
	int sumElementsOfMassive(int* arr, int length);
	int findMinMaxElementFromMassive(int* arr, int length);

}